#basic project setup
cmake_minimum_required(VERSION 3.0)
project (OpenGLProjectII)

#include libraríes
#OpenGL
find_package(OpenGL REQUIRED)
#GLEW
find_package(GLEW REQUIRED)
#SDL
INCLUDE(FindPkgConfig)
PKG_SEARCH_MODULE(SDL2 REQUIRED sdl2)

#add all source dirs
add_subdirectory(src)
