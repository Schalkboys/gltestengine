#include "Engine.hpp"
#include <iostream>
#include <GL/glew.h>
#include <SDL2/SDL.h>

using namespace std;

bool enginePreInit() {
        cout << "======ENGINE PRE INIT======" << endl;
        //----------SDL--------------------
        if(SDL_Init(SDL_INIT_VIDEO) != 0) {
                cout << "SDL_Init() failed " 
                     << SDL_GetError()
                     << endl;

                return false;
        }
     	//use OpenGL 4.5
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION,  4);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);
      
        SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);

	return true;
}

bool enginePostInit() {
        //---------GLEW------------
        glewExperimental = GL_TRUE;
        if(glewInit() != GLEW_OK) {
                cout << "Unable to initialize GLEW!"
                     << endl;
      
                return false;
        }

	return true;
}

bool engineRender() {
	//closes all displays if the x buton was pressed
	SDL_Event event;
	while(SDL_PollEvent(&event)) {
		if(event.type == SDL_QUIT) 
			return false;	
	}
	
	return true;
}

void engineShutdown() {
	SDL_Quit();
}
