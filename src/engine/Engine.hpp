bool enginePreInit();
bool enginePostInit();
bool engineRender();
void engineShutdown();

/** This macro automatically defines the engines main loop
 * 
 *  It is required that there are four functions defined
 *  called:
 * 
 *  void enginePreInit();  initialize Displays in here
 *  void enginePostInit(); initialize Game specific systems    
 *  void render();         for code that executes once every frame
 *  void shutdown();       for deallocating memory once the app finished running
 */
#define ENGINE_MAIN int main() {\
			bool _engineRunning = enginePreInit(); \
			preInit();\
			_engineRunning = enginePostInit();\
			postInit();\
			\
			while(_engineRunning) {\
				render();\
				_engineRunning = engineRender();\
			}\
			shutdown();\
			engineShutdown();\
			return 0;\
		    }

