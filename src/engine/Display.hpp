#include <SDL2/SDL.h>
#include <iostream>
#include <string>

class Display {
private:
	SDL_Window *m_window;
	SDL_GLContext m_glContext;
public:
	//don't use constructor or destructor
	Display() {}
	~Display() {}
	
	//use this in the corresponding functions 
	bool init(int width, int height, const char *name);
	void render();
	void shutdown();

	SDL_Window *getSDLWindow() {return m_window;};
	SDL_GLContext getSDLGLContext() {return m_glContext;};
};
