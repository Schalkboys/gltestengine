#include <GL/glew.h>

struct ShaderPaths {
	const char *vertShader;
	const char *tessContShader;
	const char *tessEvalShader;
	const char *geomShader;
	const char *fragShader;
};

class Shader {
private:
	GLuint m_program;

public:
	int init(ShaderPaths shaderPaths);
	void bind();
	void shutdown();
	//do not use
	Shader() {}
	~Shader(){}

};
