#include "Shader.hpp"
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

//define helper functions
const char* loadFile(const char* fileName);
bool checkCompileError(GLuint shader);
char* logCompileError(GLuint shader);

int Shader::init(ShaderPaths shaderPaths) {	

	//we will only continue if there is at least a vertex and a fragment shader present
	if(shaderPaths.vertShader == "" ||
	   shaderPaths.fragShader == "") { 
		cerr << "Vertex shader and fragment shader must be specified!"
		     << endl;

		return -1;
	}
	
	auto vertShaderFile = loadFile(shaderPaths.vertShader);
	auto fragShaderFile = loadFile(shaderPaths.fragShader);
	//create and compile vertex Shader
	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &vertShaderFile, NULL);
	glCompileShader(vertexShader);
	//error checking
	if(checkCompileError(vertexShader)) {
		cerr << "Error compiling vertex shader: " 
		     << logCompileError(vertexShader)
		     << endl;
		return -1;
	}
	//create and compile fragment shader
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fragShaderFile, NULL);
	glCompileShader(fragmentShader);
	//error checking
	if(checkCompileError(fragmentShader)) {
		cerr << "Error compiling fragment shader: "
		     << logCompileError(fragmentShader)
		     << endl;
		return -1;
	}

	//check for and comile geometry shader
	GLuint geometryShader = 0;
	if(shaderPaths.geomShader != "") {
		auto geomShaderFile = loadFile(shaderPaths.geomShader);
		
	 	geometryShader = glCreateShader(GL_GEOMETRY_SHADER);
		glShaderSource(geometryShader, 1, &geomShaderFile, NULL);
		glCompileShader(geometryShader);
		//error checking
		if(checkCompileError(geometryShader)) {
			cerr << "Error compiling geometry shader: "
			     << logCompileError(geometryShader)
			     << endl;
			return -1;
		}
	} else {
		cout << "No geometry shader specified." << endl;
	}	

	GLuint tessContShader = 0;
	GLuint tessEvalShader = 0;
	if(shaderPaths.tessContShader != "" &&
	   shaderPaths.tessEvalShader != "") {
		//compile control shader
		auto tessContShaderFile = loadFile(shaderPaths.tessContShader);
		
		tessContShader = glCreateShader(GL_TESS_CONTROL_SHADER);
		glShaderSource(tessContShader, 1, &tessContShaderFile, NULL);
		glCompileShader(tessContShader);
		//and aigain we check for errors
		if(checkCompileError(tessContShader)) {
			cerr << "Error compiling tesselation evaluation shader: "
			     << logCompileError(tessContShader)
			     << endl;
			return -1;
		}
		//compile evaluation shader
		auto tessEvalShaderFile = loadFile(shaderPaths.tessEvalShader);
		
		tessEvalShader = glCreateShader(GL_TESS_EVALUATION_SHADER);
		glShaderSource(tessEvalShader, 1, &tessEvalShaderFile, NULL);
		glCompileShader(tessEvalShader);
		//error checking yet again
		if(checkCompileError(tessEvalShader)) {
			cerr << "Error compiling tesselation evaluation shader: "
			     << logCompileError(tessEvalShader)
			     << endl;
			return -1;
		}
	}
	//finally create the shader program
	m_program = glCreateProgram();
	glAttachShader(m_program, vertexShader);
	if(tessContShader != 0)
		glAttachShader(m_program, tessContShader);
	if(tessEvalShader != 0)
		glAttachShader(m_program, tessEvalShader);
	if(geometryShader != 0)
		glAttachShader(m_program, geometryShader);
	glAttachShader(m_program, fragmentShader);
	
	glLinkProgram(m_program);
	//check for linker errors
	auto isLinked = 0;
	glGetProgramiv(m_program, GL_LINK_STATUS, &isLinked);
	if(isLinked == GL_FALSE) {
		auto length = 0;
		glGetProgramiv(m_program, GL_INFO_LOG_LENGTH, &length);
		
		auto infoLog = new char[length];
		glGetProgramInfoLog(m_program,
				 length,
				 NULL,
				 infoLog);
		return -1;
	}
	
	glDeleteShader(vertexShader);
	glDeleteShader(tessContShader);
	glDeleteShader(tessEvalShader);
	glDeleteShader(geometryShader);
	glDeleteShader(fragmentShader);

	return 0;
}

void Shader::shutdown() {
	glDeleteProgram(m_program);
}

void Shader::bind() {
	glUseProgram(m_program);
}


//helper functions
const char* loadFile(const char* fileName) {
	string line;
	string output;
	ifstream file(fileName);
	
	if(!file.is_open()) {
		cerr << "Unable to open \""
		     << fileName << "\"!"
		     << endl;
		return "";
	}

	while(getline(file, line)) {
		output.append(line + "\n");
	}
	
	cout << output << endl;
	//makes it opengl compatible
	const char* returnValue = 
		output.c_str();

	return returnValue;
}

bool checkCompileError(GLuint shader) {
	auto isCompiled = 0;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
	
	if(isCompiled == GL_TRUE)
		return false;
	else 
		return true;
}

char* logCompileError(GLuint shader) {
	auto maxLength = 0;
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);
	
	auto errorLog = new char[maxLength];
	glGetShaderInfoLog(shader, maxLength, NULL, &errorLog[0]);
	
	return errorLog;
}

