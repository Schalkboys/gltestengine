#include "Display.hpp"
#include <string>
#include <iostream>

bool Display::init(int width, int height, const char *name) { 
	m_window = SDL_CreateWindow(name, 
			0, 0, 
			width, height,
			SDL_WINDOW_OPENGL);
	
	if(m_window == nullptr) {
		std::cerr << "ERROR: Window not initialized "
			  << SDL_GetError()
			  << std::endl;
		
		return false;	
	}
	
	m_glContext = SDL_GL_CreateContext(m_window);
	return true;
}

void Display::render() {
	SDL_GL_SwapWindow(m_window);
}

void Display::shutdown() {
	SDL_GL_DeleteContext(m_glContext);
	SDL_DestroyWindow(m_window);
}
