#include <GL/glew.h>
#include <iostream>

#include "Engine.hpp"
#include "Display.hpp"
#include "Shader.cpp"

using namespace std;

//display settings
constexpr const int DISPLAY_WIDTH  = 800;
constexpr const int DISPLAY_HEIGHT = 600;

constexpr const char *DISPLAY_TITLE = "OpenGLProjectII";
//lifecycle functions
bool init();
bool render();
void shutdown();

//global vars
Display g_display;
Shader g_testShader;

//for calling the shader
GLuint g_vao;

bool preInit() {
	cout << "============PRE INIT========================" << endl;
	//---------g_display-----------------------------------------------
	if(!g_display.init(DISPLAY_WIDTH, DISPLAY_HEIGHT, DISPLAY_TITLE)) {
		cout << "g_display.init() failed!" << endl;

		return false;
	}
}

bool postInit() {
	cout << "=============POST INIT======================" << endl;
	//--------g_testShader---------------------------------
	ShaderPaths testPaths = {"test.vs",
				 "",
				 "",
				 "",
			 	 "test.fs"};
	g_testShader.init(testPaths);
	
	cout << "after initializing shader" << endl;

	glCreateVertexArrays(1, &g_vao);
	glBindVertexArray(g_vao);
	return true;
}

bool render() {
	//clear the screen
	glClearColor(0, 0.75, 0.75, 1);
	glClear(GL_COLOR_BUFFER_BIT);

	g_testShader.bind();
	
	glDrawArrays(GL_TRIANGLES, 0, 3);
	
	g_display.render();
}

void shutdown() {
	cout << "======SHUTDOWN======" << endl;
	glDeleteVertexArrays(1, &g_vao);
	g_testShader.shutdown();
	g_display.shutdown();
}

ENGINE_MAIN

